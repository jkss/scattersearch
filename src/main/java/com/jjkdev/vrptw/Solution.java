package com.jjkdev.vrptw;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Solution 
{
	private ArrayList<Route> routes = new ArrayList<>();
	private Depot depot;
	private int nodesInRoute;

	
	public Solution(Depot depot, int nodesInRoute) { this.depot = depot; this.nodesInRoute = nodesInRoute; }
	public Solution(Solution solution) {this.routes = solution.routes; this.depot = solution.depot; this.nodesInRoute = solution.nodesInRoute; }
	public double length()
	{
		int length = 0;
		int lastIndex = 0;
		
		for(Route route : this.routes)
		{
			
			length += Route.distance(this.depot, route.getNodes().get(0));
			
			for(int i = 0; i < route.getNodes().size(); i++)
			{ 
				if( i + 1 < route.getNodes().size() ) length += Route.distance(route.getNodes().get(i), route.getNodes().get(i+1));
				else lastIndex = i;
			}
			
			length += Route.distance(route.getNodes().get( lastIndex ), this.depot);
		}
		
		return length;
	}
	
	public Depot getDepot() { return depot; }
	public void setDepot(Depot data) { this.depot = data; }
	
	public ArrayList<Route> getRoutes() { return routes; }
	public void setRoutes(ArrayList<Route> data) { this.routes = data; }
	
	public void addRoute(Route data) { this.routes.add(data); }
	public void removeRoute(Route data) { this.routes.remove(data); }
	
	public void printRoutes()
	{
		int routeNo = 1;
		
		for(Route route : this.routes)
		{
			if( route.getNodes().size() > 0 )
			{
				System.out.println("+=====================");
				System.out.println("| Route #" + routeNo);
				System.out.println("|---------------------");
				for(Node node : route.getNodes())
				{
					System.out.println("| Customer #" + node.getId() + " (" + node.getX() + ", " + node.getY() + ")");
					
				}
				System.out.println("+---------------------");
				
				++routeNo;
			}
		}
	}
	
	/**
	 * Needs improvement in order to find shortest path.
	 */
	public void improveRoutePathing()
	{
		for(Route route : this.routes)
		{
			int indexOfShortest = 0;
			
			/* Find best first element */
			for(int i = 0; i < route.getNodes().size(); i++)
			{
				if(Route.distance(this.depot, route.getNodes().get(i)) <= Route.distance(this.depot, route.getNodes().get(indexOfShortest)))
				{
					indexOfShortest = i;
				}
			}
			
			route.swapNodes(route.getNodes().get(0), route.getNodes().get(indexOfShortest));
			
			
			/* Find best places for other elements */
			if(this.routes.size() > 2)
			{
				for(int i = 1; i < route.getNodes().size(); i++)
				{
					indexOfShortest = i;
					
					for(int j = i; j < route.getNodes().size() - 1; j++)
					{
						if( Route.distance(route.getNodes().get(i), route.getNodes().get(j)) <= Route.distance(route.getNodes().get(i), route.getNodes().get(indexOfShortest)) )
						{
							indexOfShortest = j;
						}
					}
					
					route.swapNodes(route.getNodes().get(i), route.getNodes().get(indexOfShortest));
				}
			}
			
		}
	}
	
	public void recombineRoutes()
	{
		int bestFit;
		
		for(int i = 0; i < this.routes.size(); i++)
		{
			bestFit = -1;
			
			if( this.routes.get(i).getNodes().size() < this.nodesInRoute )
			{
				for(int j = 0; j < this.routes.size(); j++)
				{
					if( this.routes.get(j).getNodes().size() < this.nodesInRoute )
					{
						if( bestFit == -1 || distanceBetweenRoutes(this.routes.get(i), this.routes.get(bestFit)) < distanceBetweenRoutes(this.routes.get(i), this.routes.get(j)) ) bestFit = j;
					}
				}
				
				if( bestFit != i )
				{
					if( Route.distance( this.routes.get(i).first(), this.routes.get(bestFit).last() ) < Route.distance( this.routes.get(bestFit).first(), this.routes.get(i).last() ) )
					{
						// place bestFit nodes in the front of i
						for(int r = this.routes.get(bestFit).getNodes().size() - 1; r >= 0; r--)
						{
							this.routes.get(i).getNodes().add( 0, this.routes.get(bestFit).getNodes().get(r) );
						}
						
						
					}
					else
					{
						// place bestFit nodes at the end of i
						for(int r = 0; r < this.routes.get(bestFit).getNodes().size(); r++)
						{
							this.routes.get(i).getNodes().add( this.routes.get(bestFit).getNodes().get(r) );
						}
					}
					
					this.routes.remove(bestFit);
					
					i = -1;
				}
				
			}
		}
			
	}
	
	public double distanceBetweenRoutes(Route A, Route B)
	{
		return Math.min( Route.distance(A.last(), B.first()), Route.distance(B.first(), A.last()) );
	}

	
}
