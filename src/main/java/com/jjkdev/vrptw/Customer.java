package com.jjkdev.vrptw;

public class Customer extends Node
{
	private boolean isAssigned;
	
	public Customer(int id, int x, int y, int demand, int readyTime, int dueTime, int serviceTime)
	{
		super(id, x, y, demand, readyTime, dueTime, serviceTime);
		
		this.setUnassigned();
	}
	
	public boolean isAssigned() { return isAssigned; }
	public void setAssigned() { this.isAssigned = true; }
	public void setUnassigned() { this.isAssigned = false; }
}
