package com.jjkdev.vrptw;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DesktopApp {

	// @Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Drawing Operations Test");

		Group root = new Group();
		Scene myScene = new Scene(root, 600, 600);

		GridPane gridPane = new GridPane();
		gridPane.setPadding(new Insets(15));
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		gridPane.setAlignment(Pos.CENTER);

		gridPane.add(new Label("Vehicles:"), 0, 0);
		TextField vehicles = new TextField();
		vehicles.setText("5");
		gridPane.add(vehicles, 1, 0);

		gridPane.add(new Label("Capacity:"), 0, 1);
		TextField capacity = new TextField();
		capacity.setText("200");
		gridPane.add(capacity, 1, 1);

		gridPane.add(new Label("Max iterations:"), 0, 2);
		TextField maxIterations = new TextField();
		maxIterations.setText("1000");
		gridPane.add(maxIterations, 1, 2);

		gridPane.add(new Label("Path:"), 0, 3);
		TextField path = new TextField();
		gridPane.add(path, 1, 3);

		Button brwoseButton = new Button("Browse");
		gridPane.add(brwoseButton, 2, 3);
		brwoseButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Open Resource File");
				File file = fileChooser.showOpenDialog(primaryStage);
				path.setText(file.getAbsolutePath());
			}
		});

		if (!path.getText().isEmpty()) {

		}

		Button aButton = new Button("Draw");
		gridPane.add(aButton, 0, 5);
		aButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Canvas canvas = new Canvas(600, 600);
				GraphicsContext gc = canvas.getGraphicsContext2D();
				Solution solution;
				try {
					solution = Problem.findBestSolution(Integer.valueOf(vehicles.getText()),
							Integer.valueOf(capacity.getText()), Integer.valueOf(maxIterations.getText()),
							path.getText());
					drawPaths(solution, primaryStage, gc);
					gridPane.add(new Label("Total length: "), 1, 85);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				root.getChildren().add(canvas);

			}
		});
		GridPane.setHalignment(aButton, HPos.LEFT);

		root.getChildren().add(gridPane);
		primaryStage.setScene(myScene);
		primaryStage.show();
	}

	private void drawPaths(Solution solution, Stage primaryStage, GraphicsContext gc) throws FileNotFoundException {

		Group root = (Group) primaryStage.getScene().getRoot();
		GridPane gridPane = new GridPane();
		gridPane.setPadding(new Insets(15));
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		gridPane.setAlignment(Pos.CENTER);

		root.getChildren().add(gridPane);
		primaryStage.getScene().setRoot(root);

		gc.scale(4, 4);
		gc.translate(20, 40);
		List<Node> pathWithDepot;
		for (int i = 0; i < solution.getRoutes().size(); i++) {
			pathWithDepot = new ArrayList<>();
			pathWithDepot.add(solution.getDepot());
			pathWithDepot.addAll(solution.getRoutes().get(i).getNodes());
			pathWithDepot.add(solution.getDepot());
			double xPoints[] = new double[pathWithDepot.size()];
			double yPoints[] = new double[pathWithDepot.size()];

			for (int j = 0; j < pathWithDepot.size(); j++) {
				xPoints[j] = pathWithDepot.get(j).getX();
				yPoints[j] = pathWithDepot.get(j).getY();
			}

			gridPane.add(new Label("Route #" + i + ": "), 8, i + 87);

			gridPane.add(new Label("Customers: "), 23, i + 87);
			for (int l = 0; l < solution.getRoutes().get(i).getNodes().size(); l++)
				gridPane.add(new Label("#" + solution.getRoutes().get(i).getNodes().get(l).getId()), 24 + l, i + 87);

			gc.setStroke(Color.rgb(new Random().nextInt(255), new Random().nextInt(255), new Random().nextInt(255)));

			double axisY = 75 + i * 5.5;
			gc.strokeLine(-15, axisY, -10, axisY);

			gc.strokePolygon(xPoints, yPoints, pathWithDepot.size());

			for (int k = 0; k < xPoints.length; k++) {
				gc.strokeOval(xPoints[k], yPoints[k], 1, 1);
			}

			gc.strokeOval(29, axisY - 1, 1, 1);

			gc.setStroke(Color.BLACK);
			gc.strokeOval(xPoints[0], yPoints[0], 1, 1);
		}

		gridPane.add(new Label(" " + solution.length()), 19, 93);
	}

}