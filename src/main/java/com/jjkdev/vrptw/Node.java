package com.jjkdev.vrptw;

public class Node
{
	private int id;
	
	private int x;
	private int y;
	
	private int demand;
	
	private int readyTime;
	private int dueTime;
	private int serviceTime;
	
	public Node(int id, int x, int y, int demand, int readyTime, int dueTime, int serviceTime)
	{
		this.setId(id);
		
		this.x = x;
		this.y = y;
		
		this.demand = demand;
		
		this.readyTime = readyTime;
		this.dueTime = dueTime;
		this.serviceTime = serviceTime;
	}

	public int getId() { return id; }
	public void setId(int data) { this.id = data; }

	public int getX() { return x; }
	public void setX(int data) { x = data; }

	public int getY() { return y; }
	public void setY(int data) { this.y = data; }

	public int getDemand() { return demand; }
	public void setDemand(int data) { this.demand = data; }

	public int getReadyTime() { return readyTime; }
	public void setReadyTime(int data) { this.readyTime = data; }

	public int getDueTime() { return dueTime; }
	public void setDueTime(int data) { this.dueTime = data; }

	public int getServiceTime() { return serviceTime; }
	public void setServiceTime(int data) { this.serviceTime = data; }

}
