package com.jjkdev.vrptw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Route
{
	private ArrayList<Node> nodes = new ArrayList<>();	
	
	public ArrayList<Node> getNodes() { return this.nodes; }
	
	public void addNode(Node data) { this.nodes.add(data); }
	public void removeNode(Node data) { this.nodes.remove(data); }
	
	public void swapNodes(Node a, Node b)
	{
		Collections.swap(this.nodes, nodes.indexOf(a), nodes.indexOf(b));
	}
	/*
	public double calculateLength()
	{
		double length = 0;
		
		for(int i = 1; i < this.nodes.size(); i++)
		{
			length += Route.distance( this.nodes.get(i - 1), this.nodes.get(i) );
		}
		
		return length;
	}
	*/
	
	public static double distance(Node A, Node B)
	{
		return Math.sqrt( Math.pow( (B.getX() - A.getX()) ,2) + Math.pow( (B.getY() - A.getY()) ,2) );
	}
	
	public Node first() { return this.nodes.get( 0 ); }
	public Node last() { return this.nodes.get( nodes.size() - 1 ); }
}
