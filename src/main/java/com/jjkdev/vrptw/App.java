package com.jjkdev.vrptw;

import java.io.FileNotFoundException;

import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application{
	@Override
	public void start(Stage primaryStage) throws Exception {
		DesktopApp app = new DesktopApp();
		app.start(primaryStage);
		
	}
	public static void main(String[] args) throws FileNotFoundException {

		//run GUI
		launch();
		
		//run commandline
		//Problem.findBestSolution(5, 200, 1000, "C:\\Users\\arm\\Desktop\\scattersearch\\scattersearch\\benchmark\\r101.txt");
		
		
	}
}