package com.jjkdev.vrptw;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem
{
	private int vehicles;
	private int capacity;
	
	private Depot depot;
	
	private int maxIteration;
	private int iterations;
	
	private ArrayList<Customer> customers = new ArrayList<Customer>();
	
	private ArrayList<Solution> solutions = new ArrayList<Solution>();
	public void addSolution(Solution data) { this.solutions.add(data); }
	public Solution bestSolution()
	{
		int indexOfBest = 0;
		
		for(int i = 1; i < this.solutions.size(); i++)
		{
			if(this.solutions.get(i).length() < this.solutions.get(indexOfBest).length()) indexOfBest = i;
		}
		
		return this.solutions.get(indexOfBest);
	}
	
	public Problem(int vehicles, int capacity, int maxIteration, String filename) throws FileNotFoundException
	{
		this.setVehicles(vehicles);
		this.setCapacity(capacity);
		this.maxIteration = maxIteration;
		this.iterations = maxIteration;
		
		this.importData(filename);
	}

	public int getCustomersCount() { return this.customers.size(); }
	
	public int getVehicles() { return this.vehicles; }
	public void setVehicles(int data) { this.vehicles = data; }
	
	public int getCapacity() { return this.capacity; }
	public void setCapacity(int data) { this.capacity = data; }
	
	public int iterationsLeft() { return this.iterations; }
	private void iterationFinished() { this.iterations = this.iterations - 1; }
	
	public Depot getDepot() { return depot; }
	public void setDepot(Depot data) { this.depot = data; }
	
	public void prepareForNewIteration()
	{
		for(Customer customer : this.customers) customer.setUnassigned();
		
		this.iterationFinished();
	}

	public void importData(String filename) throws FileNotFoundException
    {
		Scanner scanner;

		scanner = new Scanner( new File(filename) );
		
		while ( scanner.hasNextLine() && scanner.hasNext() )
		{
			String currentRow = scanner.nextLine();
			String[] column = currentRow.split(" ");
			
			//System.out.println(currentRow);
			
			if( Integer.parseInt(column[0]) == 0 )
			{
				this.setDepot(new Depot(
						  Integer.parseInt(column[0])
						, Integer.parseInt(column[1])
						, Integer.parseInt(column[2]) 
						, Integer.parseInt(column[3]) 
						, Integer.parseInt(column[4]) 
						, Integer.parseInt(column[5]) 
						, Integer.parseInt(column[6]) 
						));
			}
			else
			{
				this.customers.add(new Customer(
						  Integer.parseInt(column[0])
						, Integer.parseInt(column[1])
						, Integer.parseInt(column[2]) 
						, Integer.parseInt(column[3]) 
						, Integer.parseInt(column[4]) 
						, Integer.parseInt(column[5]) 
						, Integer.parseInt(column[6]) 
						));
			}
		} 
		
		scanner.close();                    
	}
	
	public int firstNotAssignedCustomerId()
	{
		for(int i = 0; i < this.customers.size(); i++)
		{
			if( !this.customers.get(i).isAssigned() ) return i;
		}
		return 0;
	}

	public Customer firstNotAssignedCustomer()
	{
		int c = this.firstNotAssignedCustomerId();

		for(int i = 0; i < this.customers.size(); i++)
		{
			if( this.customers.get(i).getDueTime() <= this.customers.get(c).getDueTime() && !this.customers.get(i).isAssigned() ) c = i;
		}
		  
		return (Customer) this.customers.get(c);
	}
	
	public Customer lastNotAssignedCustomer()
	{
		int c = this.firstNotAssignedCustomerId();

		for(int i = 0; i < this.customers.size(); i++)
		{
			if( this.customers.get(i).getDueTime() > this.customers.get(c).getDueTime() && !this.customers.get(i).isAssigned() ) c = i;
		}
		  
		return (Customer) this.customers.get(c);
	}

	public boolean clientUnassigned()
	{
		for( Customer customer : this.customers)
		{
			if( !customer.isAssigned() ) return true;
		}
		return false;
	}
	
	public Route generateDiverseSet()
	{
	  int count = 0;
	  Route diverseSet = new Route();

	  for(int j = 0; j < this.customers.size(); j++)
	  {
	    if(count < (int)(this.customers.size() / this.vehicles) && !this.customers.get(j).isAssigned() )
	    {
	      if(this.customers.get(j).getDueTime() <= this.firstNotAssignedCustomer().getDueTime() + Math.random() * Math.abs(this.firstNotAssignedCustomer().getDueTime() - this.lastNotAssignedCustomer().getDueTime()) || this.firstNotAssignedCustomer().getId() == this.lastNotAssignedCustomer().getId() )
	      {
	        diverseSet.addNode( this.customers.get(j) );

	        this.customers.get(j).setAssigned();
	        ++count;
	      }
	    }
	  }
	  return diverseSet;
	}
	
	public static Solution findBestSolution(int vehicles, int capacity, int maxIterations, String path)
			throws FileNotFoundException {

		Problem problem = null;
		try {

			problem = new Problem(vehicles, capacity, maxIterations, path);

			while (problem.iterationsLeft() > 0) {
				Solution solution = new Solution(problem.getDepot(),
						(int) (problem.getCustomersCount() / problem.getVehicles()));

				while (problem.clientUnassigned()) {
					Route route = problem.generateDiverseSet();
					if (route != null)
						solution.addRoute(route);
				}

				solution.improveRoutePathing();
				solution.recombineRoutes();
				solution.improveRoutePathing(); // OPTIONAL BUT SLIGTHLY REDUCES TOTAL LENGTH

				problem.addSolution(solution);
				problem.prepareForNewIteration();
			}

			System.out.println("length=" + problem.bestSolution().length());
			problem.bestSolution().printRoutes();

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return problem.bestSolution();

	}
	
}
